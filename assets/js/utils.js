// https://learnbatta.com/blog/how-to-add-image-in-select-options-html-93/
// https://stackoverflow.com/a/32866520
// Thanks to Jay Rizzi and learnBATTA!
function formatSelect2ImageData(data){
	if (data.disabled || data.loading || data.element.dataset.imageSource === undefined) {
		return data.text;
	}
	
	return $(`<span class="select2-image-wrapper"><img src="${data.element.dataset.imageSource}" class="select2-image" alt="${data.text}"><span class="select2-text">${data.text}</span></span>`);
}


function splitStringIntoLines(context, text, maxLength, maxLines) {
	let words = text.split(/( |\n|\r\n)/g);
	
	let currentLine = [];
	let lines = [[]];
	let linePos = 0;
	let breakCount = 0;
	
	for (word of words) {
		if (word == " " || word == "") {
			continue;
		}
		
		if (word == "\n" || word == "\n\r") {
			if (breakCount !== maxLines - 1) {
				breakCount++;
				linePos++;
				
				currentLine = [];
				
				lines.push([]);
			}
			
			continue;
		}
		
		currentLine.push(word);
		
		if (
			context.measureText(currentLine.join(" ")).width
			> maxLength
		) {
			linePos++;
			
			currentLine = [word];
		}
		
		if (lines.length - 1 < linePos) {
			lines.push([]);
		}
		
		lines[linePos].push(word);
	}
	
	return lines;
}

// https://stackoverflow.com/a/5915122
// Thanks to Kelly and Corey!
function getRandomArrayElement(array) {
	return array[Math.floor(Math.random()*array.length)];
}
