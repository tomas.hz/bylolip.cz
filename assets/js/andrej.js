NAMES = {
	"andrej": {
		name: "Andrej",
		position: "Expremiér",
		image: "assets/images/slogans/za-babise-bylo-andrejovi-lip.png",
		quotes: [
			{
				quote: "S tou kontrolou si snad dělaj srandu.",
				reasoning: "Kumulace mediální, ekonomické a politické moci? To je nebezpečná věc. Je tu velká hrozba zneužití. Piráti proti střetu zájmů politiků vždy bojovali a vždy bojovat budou."
			},
			{
				quote: "Kdy budu moct zase všechny zadlužovat?",
				reasoning: "Piráti šli do voleb 2021 s jasným programem - snížit zadlužení státu a zefektivnění státní správy. "
			},
			{
				quote: "Kdy budu moct stavět další čapák ze státního?",
				reasoning: "Peníze daňových poplatníků na byznys politiků? To tedy ne. Piráti brojí proti zneužívání dotací a flagrantnímu střetu zájmů."
			},
			{
				quote: "Kdy zas někoho budou zajímat moje lži?",
				reasoning: "Andrej Babiš vynaložil 90 milionů na kampaň do Sněmovny 2021 z velké části na potopení Pirátů. Nezdařilo se a Piráti zastavili orbánizaci Česka Andrejem Babišem."
			},
			{
				quote: "Vládl jsem s podporou fašistů a komunistů.",
				reasoning: "Zveřejňujeme závaznou povolební strategii ještě před volbami. Nikdy jsme nepodporovali fašisty, komunisty nebo populisty.",
				position: "Vládce s mimozemšťany",
				image: "assets/images/slogans/vladlo-se-krasne-s-kymkoli.png"
			},
			{
				quote: "Sjednotil jsem fašisty a komunisty.",
				reasoning: "Piráti vždy upozorňovali na neformální koalici ve Sněmovně (ANO + SPD + Komunisti + ČSSD) - prošlo díky ní spoustu špatných zákonů, naopak naše návrhy vždy tato koalice shodila.",
				position: "Ničitel demokracie"
			},
			{
				quote: "Kdy mi zaplatíte další Čapí hnízdo?",
				reasoning: "Česká republika ze státního rozpočtu proplatila Babišovi (Agrofertu či dceřinkám) peníze na projekty, které odmítla EU na základě auditu. Z iniciativy Pirátů.",
				position: "Dojič dotací",
				image: "assets/images/slogans/za-me-se-drobne-neresily.png"
			},
			{
				quote: "Podojil jsem stát, rozdojím i kozla.",
				reasoning: "Babiš byl velmi vynalézavý v projektech, které mu zaplatil český stát z rozpočtu. Další byly offshore skořápky, za které si pořídil vilu ve Francii.",
				position: "Vykuk",
				image: "assets/images/slogans/vladl-jsem-a-smelil.png"
			},
			{
				quote: "Donášel jsem pro StB a teď jsem jedním z vás.",
				reasoning: "Andrej Babiš prohrál soud a je tak potvrzeným konfidentem StB, cestoval za minulého režimu do zahraničí, profitoval na privatizaci a měl vždy více informací a možností, než ostatní. Nehrál fér.",
				position: "Agent StB",
				image: "assets/images/slogans/prikryl-jsem-rozsudek.png"
			},
			{
				quote: "Lhář, dotační podvodník a StBák mohl být i premiérem.",
				reasoning: "Andrej Babiš prohrál soud a je tak potvrzeným konfidentem StB, cestoval za minulého režimu do zahraničí, profitoval na privatizaci a měl vždy více informací a možností, než ostatní. Nehrál fér.",
				position: "Agent StB",
				image: "assets/images/slogans/za-me-to-nevadilo.png"
			}
		]
	},
	"daniel": {
		name: "Daniel",
		position: "Dotační podovodník",
		image: "assets/images/slogans/za-babise-bylo-podvodnikum-lip.png",
		quotes: [
			{
				quote: "Kdy už udělaj něco pro šmejdy?",
				reasoning: "Piráti dlouhodobě upozorňovali a upozorňují na Babišův střet zájmů, kdy jeho lidé rozhodují o tom, zda dostane dotace, nebo ne. Střet zájmů politiků řešíme ve Sněmovně stále."
			}
		]
	},
	"pavel": {
		name: "Pavel",
		position: "Kasař",
		quotes: [
			{
				quote: "Kdy už budu moct zase krást?",
				reasoning: ""
			}
		]
	},
	"putin": {
		name: "Putin",
		position: "Vyhlazovač skladů",
		image: "assets/images/slogans/za-babise-bylo-putin-lip.png",
		quotes: [
			{
				quote: "Kdy už budu moct zase bombit?",
				reasoning: "Piráti dlouhodobě poukazují na propojení dua Babiš - Zeman na ruského prezidenta, který začal válku. Plyn, ropa - závislost ČR na Rusku za Babiše vzrostla. "
			}
		]
	},
	"schillerova": {
		name: "Schillerová",
		position: "Exministryně",
		image: "assets/images/slogans/za-babise-bylo-schillerove-lip.png",
		quotes: [
			{
				quote: "Kdy dostanu zase pávy a 2 míče na fotky?",
				reasoning: "Piráti v rámci transparentnosti upozornili na to, že fotograf Aleny Schillerové byl placen ze státních peněz pro osobní PR exministryně a hnutí ANO obecně. "
			},
			{
				name: "Alena",
				quote: "Byt v Pařížské, řidič a pávi za 2 mega. To byly časy!",
				reasoning: "Byty v Pařížské z iniciativy MMR využívají ukrajinské uprchlice.",
				position: "Instagramová modelka",
				image: "assets/images/slogans/za-babise-mi-to-sluselo-vic.png"
			}
		]
	},
	"vondracek": {
		name: "Vondráček",
		position: "Expředseda sněmovny",
		image: "assets/images/slogans/za-babise-bylo-vondrackovi-lip.png",
		quotes: [
			{
				quote: "Kdy se zas dostanu k pořádný lichvě?",
				reasoning: "Piráti poukázali na aktivity R. V. coby právníka lichvářské firmy, včetně aktivní participace na obírání lidí. "
			},
			{
				quote: "Chci si zase skočit na stole.",
				reasoning: "Radek Vondráček hrál pro svou návštěvu na kytaru na pultu, u kterého sedí členové vlády. Piráti takovou dehonestaci odsoudili. ",
				position: "Kytarista",
				image: "assets/images/slogans/za-babise-se-juchalo-vsude.png"
			},
			{
				quote: "Lichva byla podnikání jako každé jiné.",
				reasoning: "Místopředseda Sněmovny nesmí mít takovou minulost v podnikání s chudobou. Radek Vondráček čelil vyšetřování za své aktivity v minulosti. Dělal advokáta lichvářům a aktivně se na byznysu podílel.",
				position: "Právník lichvářů",
				image: "assets/images/slogans/za-babise-jsem-byl-v-suchu.png"
			}
		]
	},
	"havlicek": {
		name: "Havlíček",
		position: "Exdvojministr",
		image: "assets/images/slogans/za-babise-bylo-havlickovi-lip.png",
		quotes: [
			{
				quote: "Dvě ministerstva jsou lepší, než žádný.",
				reasoning: "Když Babišové vládě došli lidi, stanul Havlíček v čele dvou ministerstev a prezentoval, že to zvládne. Piráti jsou proti kumulaci funkcí, toto byl velmi závažný případ.",
				image: "assets/images/slogans/za-babise-se-nespalo.png"
			},
			{
				quote: "Kdy zase vykolejí vlaky?",
				reasoning: "Během ministrování Havlíčka vykolejilo za krátké období hodně vlaků, poukazuje to na podfinancování resortu dopravy, obzvláště Českých drah",
				position: "Exministr dopravy",
				image: "assets/images/slogans/za-babise-jsem-ustal-i-tohle.png"
			}
		]
	},
	"slepice": {
		name: "Slepice z Agrofertu",
		position: "Slepice",
		image: "assets/images/slogans/za-babise-bylo-slepici-lip.png",
		quotes: [
			{
				quote: "Kolik toho ještě snesu?",
				reasoning: ""
			}
		]
	},
	"honza": {
		name: "Honza",
		position: "Zaklekávač z finančáku",
		image: "assets/images/slogans/za-babise-bylo-havlickovi-lip.png",
		quotes: [
			{
				quote: "Kdy zas budu moct šikanovat?",
				reasoning: "Firmy neloajální Agrofertu čelily vícenásobným kontrolám finančního úřadu jako trest za neloajalitu. Piráti toto téma vynesli do mediálního prostoru"
			}
		]
	},
	"faltynek": {
		name: "Faltýnek",
		position: "Šedá eminence",
		image: "assets/images/slogans/za-babise-bylo-faltynkovi-lip.png",
		quotes: [
			{
				quote: "Kdy zas budu moct kmotřit?",
				reasoning: "Všechna důležitá zákulisní jednání vedl za Babiše Faltýnek. Mafiánské praktiky. "
			},
			{
				quote: "Kdy mi policie vrátí deníček?",
				reasoning: "Faltýnkův deníček obsahuje mnoho potvrzených informací o domlouvání zakázek jak na celostátní, tak lokální úrovni, např. Správa silnic Olomouckého kraje.",
				position: "Spisovatel a šmelinář",
				image: "assets/images/slogans/za-babise-se-kradlo-lip.png"
			}
		]
	},
	"vojtech": {
		name: "Vojtěch Filip",
		position: "Komunista",
		image: "assets/images/slogans/za-babise-bylo-falmerovi-lip.png",
		quotes: [
			{
				quote: "Kdy zas budu důležitej?",
				reasoning: "I díky Pirátům vypadli komunisté poprvé z české Poslanecké Sněmovny."
			}
		]
	},
	"ryba": {
		name: "Ryba",
		position: "Ryba",
		image: "assets/images/slogans/za-babise-bylo-rybe-lip.png",
		quotes: [
			{
				quote: "Kdy už budu moct zase kloktat kyanid?",
				reasoning: "Únik chemikálií do Bečvy ukazuje nepřímo na Babišovu chemičku. Richard Brabec, exministr životního prostředí, aktivně bránil vyšetřování v prvních hodinách a dnech. "
			}
		]
	},
	"hala": {
		name: "Hala pro Sáblíkovou",
		position: "Hala",
		image: "assets/images/slogans/za-babise-bylo-hale-lip.png",
		quotes: [
			{
				quote: "Za Babiše jsem byla aspoň hypotetická!",
				reasoning: ""
			}
		]
	},
	"adam": {
		name: "Adam Vojtěch",
		position: "Exministr zdravotnictví",
		image: "assets/images/slogans/za-babise-bylo-adamovi-lip.png",
		quotes: [
			{
				image: "assets/images/slogans/je-mi-tam-zima.png",
				quote: "Musím zpívat ve Finsku.",
				reasoning: "Adam Vojtěch se za věrné služby Andreji Babišovi stal velvyslancem ve Finsku."
			}
		]
	},
	"tomio": {
		name: "Okamura",
		position: "Opoziční populista",
		image: "assets/images/slogans/za-babise-bylo-tomiovi-lip.png",
		quotes: [
			{
				image: "assets/images/slogans/ani-si-bez-nas-neprdli.png",
				quote: "Drželi jsme vládu pod krkem.",
				reasoning: "Důležitá hlasování ve Sněmovně protlačila vláda díky hlasům nepřiznané koalice, hlavně s komunisty a SPD."
			},
			{
				quote: "Zakážeme mikrovlnky!",
				reasoning: ""
			},
			{
				quote: "Klády na koleje!",
				reasoning: ""
			},
			{
				quote: "Zákaz všeho nečeského. Počkat! Nééééeeee......",
				reasoning: ""
			},
			{
				quote: "Určitě možná s váma tak uplně nesouhlasím",
				reasoning: ""
			},
			{
				quote: "Lžeme víc než Andrej!",
				reasoning: ""
			},
			{
				quote: "Všechno překroutíme.",
				reasoning: ""
			},
			{
				quote: "Stvořil jsem Volného!",
				reasoning: ""
			}
		]
	},
	"michalek": {
		name: "Jakub Michálek",
		position: "Pirát",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Bojujeme proti politikům zneužívajícím svou moc!",
				reasoning: ""
			},
			{
				quote: "Chceme skutečnou spravedlnost!",
				reasoning: ""
			},
			{
				quote: "Chceme efektivní soudy!",
				reasoning: ""
			},
			{
				quote: "Nedovolíme Babišovi připravit nás o 180 miliard z EU!",
				reasoning: ""
			}
		]
	},
	"bartos": {
		name: "Ivan Bartoš",
		position: "Pirát",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Stát nemá házet lidem klacky pod nohy!",
				reasoning: ""
			},
			{
				quote: "Občanka na pár kliknutí!",
				reasoning: ""
			},
			{
				quote: "Daně na pár kliknutí!",
				reasoning: ""
			},
			{
				quote: "Zachránili jsme 311 stavebních úřadů, které chtěl Babiš zrušit!",
				reasoning: ""
			},
			{
				quote: "Digitalizujem Česko!",
				reasoning: ""
			}
		]
	},
	"richterova": {
		name: "Olga Richterová",
		position: "Pirátka",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Bojujeme za valorizaci rodičovského příspěvku!",
				reasoning: ""
			},
			{
				quote: "Zastáváme se slabších!",
				reasoning: ""
			},
			{
				quote: "Aby nikdo nezůstal na vedlejší koleji!",
				reasoning: ""
			},
			{
				quote: "Prosadíme manželství pro všechny!",
				reasoning: ""
			},
			{
				quote: "Pomáháme lidem v krizi!",
				reasoning: ""
			},
			{
				quote: "Podporu rodinám!",
				reasoning: ""
			}
		]
	},
	"kocmanova": {
		name: "Klára Kocmanová",
		position: "Pirátka",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Dbáme na lidská práva!",
				reasoning: ""
			},
			{
				quote: "Ochráníme životní prostředí!",
				reasoning: ""
			},
			{
				quote: "Prosadíme manželství pro všechny!",
				reasoning: ""
			},
			{
				quote: "Konec fosílií nejen ve spalování!",
				reasoning: ""
			},
			{
				quote: "Oběti násilí je nutné chránit!",
				reasoning: ""
			},
			{
				quote: "Odvaha postavit se za přírodu a za lidi!",
				reasoning: ""
			},
			{
				quote: "Konec kastrací transgender lidí!",
				reasoning: ""
			}
		]
	},
	"lipavsky": {
		name: "Jan Lipavský",
		position: "Pirát",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Pomáháme Ukrajině!",
				reasoning: ""
			},
			{
				quote: "Bráníme Ukrajinu, bráníme sebe!",
				reasoning: ""
			},
			{
				quote: "My jsme NATO. My jsme EU.",
				reasoning: ""
			},
			{
				quote: "Obnovujeme tradici havlovské zahraniční politiky!",
				reasoning: ""
			}
		]
	},
	"salomoun": {
		name: "Michal Šalomoun",
		position: "Pirát",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Mrazíme majetek ruských oligarchů!",
				reasoning: ""
			},
			{
				quote: "Kontrolujeme zákony, aby byly prospěšné!",
				reasoning: ""
			},
			{
				quote: "Zabavujeme majetek ruských válečníků!",
				reasoning: ""
			}
		]
	},
	"hrib": {
		name: "Zdeněk Hřib",
		position: "Pirát",
		image: getRandomArrayElement([
			"assets/images/slogans/pirati-maji-odvahu-delat-veci-spravne.png",
			"assets/images/slogans/pirati-maji-odvahu-delat-co-je-spravne.png"
		]),
		disableFun: true,
		quotes: [
			{
				quote: "Opravujeme Barrandovský most!",
				reasoning: ""
			},
			{
				quote: "V Praze se neflákáme!",
				reasoning: ""
			},
			{
				quote: "Máme vizi!",
				reasoning: ""
			},
			{
				quote: "Stavíme byty!",
				reasoning: ""
			},
			{
				quote: "Napravujeme nečinnost předchozích vlád v Praze!",
				reasoning: ""
			},
			{
				quote: "Máme odvahu porvat se za Prahu!",
				reasoning: ""
			},
			{
				quote: "Máme odvahu řídit Prahu správně!",
				reasoning: ""
			}
		]
	}
}

class Babis {
	constructor(canvasId) {
		this.canvas = document.getElementById(canvasId);
		this.context = this.canvas.getContext("2d");
	}
	
	redrawing = false;
	overlayTextImage = null;
	overlaySource = null;
	person = null;
	
	primaryText = "";
	nameText = "";
	tomioText = null;
	
	async redrawCanvas() {
		if (this.redrawing === true) {
			return;
		}

		this.redrawing = true;
		
		let personHeight = this.canvas.height;
		const personMaxWidth = this.canvas.width / 2;
		
		const primaryTextPaddingSides = this.canvas.width * 0.05;
		const primaryTextPaddingTop = this.canvas.height * 0.1;
		let primaryFontSize = this.canvas.height * 0.1;
		const primaryTextInnerPadding = primaryFontSize * 0.2;
		
		const nameTextBottomPadding = this.canvas.height * 0.04;
		let nameFontSize = this.canvas.height * 0.035;
		
		let imageOffset = 0;
		
		this.context.fillStyle = "#ffffff";
		this.context.fillRect(
			0, 0,
			this.canvas.width, this.canvas.height
		);
		
		let personWidth = 0;
		let personPositionOffset = 0;

		if (this.person !== null) {
			personWidth = this.person.width * (personHeight / this.person.height);
			
			if (personWidth > personMaxWidth) {
				personPositionOffset = personWidth - personMaxWidth;
			}
			
			this.context.drawImage(
				this.person,
				this.canvas.width - personWidth + personPositionOffset, this.canvas.height - personHeight,
				personWidth, personHeight
			);
			
			personWidth -= personPositionOffset;
		}
		
		if (this.primaryText !== "") {
			this.context.font = `normal 600 ${primaryFontSize}px 'Inter'`;
			
			let textX = primaryTextPaddingSides;
			
			if (this.tomioText !== null) {
				this.context.textAlign = "center";
				this.context.fillStyle = "#162f4d";
			} else {
				this.context.textAlign = "left";
				this.context.fillStyle = "#000000";
			}
			
			let primaryLines = [];
			
			do {
				primaryLines = splitStringIntoLines(
					this.context,
					this.primaryText.toUpperCase(),
					this.canvas.width - 2 * primaryTextPaddingSides - personWidth,
					2
				);
				
				if (
					primaryLines.length
					> 2
				) {
					primaryFontSize -= 2;
					imageOffset += 4;
					
					this.context.font = `normal 600 ${primaryFontSize}px 'Inter'`;
				}
			} while (primaryLines.length > 2);
			
			if (this.tomioText !== null) {
				textX = (this.canvas.width - personWidth - personPositionOffset) / 2;
			}
			
			let currentLineY = primaryTextPaddingTop + primaryFontSize;
			
			for (let line of primaryLines) {
				this.context.fillText(
					line.join(" "),
					textX,
					currentLineY
				);
				
				currentLineY += primaryFontSize + primaryTextInnerPadding;
			}
		}
		
		this.context.textAlign = "left";
		
		if (this.tomioText !== null) {
			let tomioFontSize = this.canvas.height * 0.2;
			let tomioOffsetTop = this.canvas.height * 0.6;
			
			this.context.font = `${tomioFontSize}px 'TrashHand'`;
			
			while (this.context.measureText(this.tomioText).width > this.canvas.width - personWidth - personPositionOffset - primaryTextPaddingSides) {
				tomioFontSize -= 2;
				tomioOffsetTop += 1;
				this.context.font = `${tomioFontSize}px 'TrashHand'`;
			}
			
			this.context.fillStyle = "#162f4d";
			
			this.context.fillText(
				this.tomioText,
				primaryTextPaddingSides,
				tomioOffsetTop
			);
			
			const superRef = this;
			
			const spdPromise = new Promise(
				resolve => {
					const spdImage = new Image();
					
					spdImage.onload = function() {
						superRef.context.drawImage(
							this,
							(superRef.canvas.width - personWidth - personPositionOffset - superRef.canvas.width / 4) / 2,
							(tomioOffsetTop) * 1.2,
							superRef.canvas.width / 4,
							this.height / (this.width / (superRef.canvas.width / 4))
						);
						
						resolve();
					}
					
					spdImage.src = "assets/images/spd.png";
				}
			);
			
			await spdPromise;
		} else {
			this.context.drawImage(
				this.overlayTextImage,
				0, 0 - imageOffset,
				this.canvas.width, this.canvas.height
			);
		}
		
		if (this.nameText !== "" && this.tomioText === null) {
			this.context.fillStyle = "#000000";
			
			this.context.font = `${nameFontSize}px 'Inter'`;
			
			while (
				this.context.measureText(this.nameText).width
				> this.canvas.width - primaryTextPaddingSides - personWidth
			) {
				nameFontSize -= 2;
				
				this.context.font = `${nameFontSize}px 'Inter'`;
			}
			
			this.context.textAlign = "right";
			
			this.context.fillText(
				this.nameText,
				this.canvas.width - primaryTextPaddingSides - personWidth,
				this.canvas.height - nameTextBottomPadding - nameFontSize
			);
			
			this.context.textAlign = "left";
		}
		
		this.redrawing = false;
	}
	
	setPrimaryText(text, skipRedraw = false) {
		this.primaryText = text;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	setNameText(text, skipRedraw = false) {
		this.nameText = text;
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	async setOverlaySource(source, skipRedraw = false) {
		this.overlaySource = source;
		
		await this.loadImages();
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
	
	async loadImages() {
		const babisRef = this;
		
		const overlayTextImageLoadPromise = new Promise(
			resolve => {
				this.overlayTextImage = new Image();
				
				this.overlayTextImage.addEventListener(
					"load",
					function() {
						babisRef.overlayTextImage = this;
						
						resolve();
					}
				);
				
				if (this.overlaySource === null) {
					this.overlayTextImage.src = getRandomArrayElement([
						"assets/images/slogans/za-babise-bylo-andrejovi-lip.png",
						"assets/images/slogans/za-babise-bylo-babisovi-lip.png",
						"assets/images/slogans/za-babise-bylo-lip-jenom-jeho-lidem.png"
					]);
				} else {
					this.overlayTextImage.src = this.overlaySource;
				}
			}
		);
		
		await overlayTextImageLoadPromise;
	}
	
	async setPerson(source) {
		const babisRef = this;
		
		const personSetPromise = new Promise(
			resolve => {
				this.person = new Image();
				
				this.person.addEventListener(
					"load",
					function() {
						babisRef.person = this;
						
						resolve();
					}
				);
				
				this.person.src = source;
			}
		);
		
		await personSetPromise;
	}
	
	setTomioText(text, skipRedraw = false) {
		if (text === "") {
			this.tomioText = null;
		} else {
			this.tomioText = text;
		}
		
		if (!skipRedraw) {
			this.redrawCanvas();
		}
	}
}
