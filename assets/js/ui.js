var babis = null;

function setTextOptions(name) {
	// https://select2.org/programmatic-control/add-select-clear-items
	
	$("#primary-text").html("");
	
	for (let quote of NAMES[name].quotes) {
		const newOption = new Option(quote.quote, quote.quote, false, false);
		newOption.dataset.reasoning = quote.reasoning;
		
		if (quote.name !== undefined) {
			newOption.dataset.name = quote.name;
		}
		
		if (quote.image !== undefined) {
			newOption.dataset.imageSource = quote.image;
		} else if (NAMES[name].image !== undefined) {
			newOption.dataset.imageSource = NAMES[name].image;
		}
		
		if (quote.position !== undefined) {
			newOption.dataset.position = quote.position;
		}
		
		$("#primary-text").append(newOption);
	}
	
	$("#primary-text").val(NAMES[name].quotes[0].quote);
	$("#primary-text").trigger("change");
}

function toggleTomio(useTomio) {
	if (useTomio) {
		$("#tomio-text").css("display", "block");
	} else {
		babis.setTomioText(null);
		$("#tomio-text").css("display", "none");
	}
}

async function setPerson(dataset) {
	if (dataset.imageSource === undefined) {
		dataset.imageSource = null;
	}
	
	await babis.setPerson(dataset.imageSource, true);
	
	if (NAMES[dataset.name].quotes[0].image !== undefined) {
		await babis.setOverlaySource(NAMES[dataset.name].quotes[0].image, true);
	} else if (NAMES[dataset.name].image !== undefined) {
		await babis.setOverlaySource(NAMES[dataset.name].image, true);
	} else {
		await babis.setOverlaySource(null, true);
	}
	
	babis.setNameText(
		(
			(NAMES[dataset.name].quotes[0].name === undefined) ? 
			NAMES[dataset.name].name :
			NAMES[dataset.name].quotes[0].name
		) + ", " + (
			(NAMES[dataset.name].quotes[0].position === undefined) ? 
			NAMES[dataset.name].position :
			NAMES[dataset.name].quotes[0].position
		),
		true
	);
	babis.setPrimaryText(NAMES[dataset.name].quotes[0].quote, true);
	
	$("#reasoning").html(NAMES[dataset.name].quotes[0].reasoning);
	setTextOptions(dataset.name);
	
	await babis.redrawCanvas();
}

$(window).ready(
	async function () {
		babis = new Babis("babis");  // :)
		
		await babis.loadImages();
		
		$("#tomio-text").on(
			"input",
			function(event) {
				babis.setTomioText(event.target.value);
			}
		);
		
		$("#primary-text").select2({
			tags: true,
			width: "100%",
		});
		
		$("#primary-text").on(
			"select2:select",
			async function(event) {
				const element = event.params.data.element;
				
				if (element === undefined) {  // Custom input
					const disableFun = NAMES[$("#person-selection").select2("data")[0].element.dataset.name].disableFun;
					
					if (disableFun === true) {
						return;
					}
					
					await babis.setOverlaySource(null, true);
					babis.setPrimaryText(event.target.value);
					$("#reasoning").html("");
				}
				
				babis.setPrimaryText(element.value, true);
				$("#reasoning").html(element.dataset.reasoning);
				
				if (element.dataset.imageSource !== undefined) {
					await babis.setOverlaySource(element.dataset.imageSource, true);
				} else {
					await babis.setOverlaySource(null, true);
				}
				
				if (element.dataset.position !== undefined || element.dataset.name !== undefined) {
					babis.setNameText(
						(
							(element.dataset.name === undefined) ?
							NAMES[$("#person-selection").select2("data")[0].element.dataset.name].name :
							element.dataset.name
						)
						+ (
							(element.dataset.position === undefined) ?
							", " + NAMES[$("#person-selection").select2("data")[0].element.dataset.position].position :
							(
								(element.dataset.position !== "") ?
								", " + element.dataset.position : ""
							)
						)
					);
				}
				
				await babis.redrawCanvas();
			}
		);
		
		$("#person-selection").select2({
			templateSelection: formatSelect2ImageData,
			templateResult: formatSelect2ImageData,
			selectionCssClass: "select2-container-medium-images",
			dropdownCssClass: "select2-container-medium-images",
			width: "100%"
		});
		
		$("#person-selection").on(
			"select2:select",
			async function(event) {
				const dataset = event.params.data.element.dataset;
				toggleTomio(dataset.name === "tomio");
				
				await setPerson(dataset);
			}
		);
		
		// https://stackoverflow.com/a/50300880
		// Thanks to Ulf Aslak!
		$("#save").on(
			"click",
			function(){
				let link = document.createElement('a');

				link.download = "Šup s tím na billboard!.png";
				link.href = document.getElementById("babis").toDataURL()

				link.click();
			}
		);
		
		$("#tomio").on(
			"click",
			async function() {
				babis.redrawing = true;
				
				babis.setTomioText("Svobodné svatby!");
				
				$("#person-selection").val("Tomio");
				$("#person-selection").trigger("change");
				
				// quick fix
				await (
					fetch("/assets/fonts/trash-hand/TrashHand.woff2")
					.then(resp => resp.arrayBuffer())
					.then(font => {
						const fontFace = new FontFace("TrashHand", font);
						document.fonts.add(fontFace);
					})
				);
				
				const personDataset = $("#person-selection").select2("data")[0].element.dataset;
				await setPerson(personDataset);
				setTextOptions(personDataset.name);
				$("#reasoning").html("");
				babis.setPrimaryText("Jsme pro sňatky gayů!", true);
				toggleTomio(true);
				await babis.setPerson("assets/people/tomio-billboard.png", true);
				
				$("#tomio-text").val("Svobodné svatby!");
				
				babis.redrawing = false;
				babis.redrawCanvas();
			}
		);
		
		$("#generate-random-image").on(
			"click",
			async function() {
				babis.redrawing = true;
				
				// https://stackoverflow.com/a/29473964
				// Thanks to Smern!
				const personSelect = document.getElementById("person-selection");
				const personItems = personSelect.getElementsByTagName("option");
				const personIndex = Math.floor(Math.random() * personItems.length);
				personSelect.selectedIndex = personIndex;
				
				await $("#person-selection").trigger("change");
				
				const personDataset = $("#person-selection").select2("data")[0].element.dataset;
				await setPerson(personDataset);
				setTextOptions(personDataset.name);
				
				toggleTomio(personDataset.name === "tomio");
				
				const textSelect = document.getElementById("primary-text");
				const textItems = textSelect.getElementsByTagName("option");
				const textIndex = Math.floor(Math.random() * textItems.length);
				textSelect.selectedIndex = textIndex;
				babis.redrawing = false;
				
				babis.setPrimaryText(textSelect.value, true);
				$("#reasoning").html(textSelect.dataset.reasoning);
				
				if (textSelect.dataset.imageSource !== undefined) {
					await babis.setOverlaySource(textSelect.dataset.imageSource, true);
				} else {
					await babis.setOverlaySource(null, true);
				}
				
				if (textSelect.dataset.position !== undefined || textSelect.dataset.name !== undefined) {
					babis.setNameText(
						(
							(textSelect.dataset.name === undefined) ?
							NAMES[$("#person-selection").select2("data")[0].textSelect.dataset.name].name :
							textSelect.dataset.name
						)
						+ (
							(textSelect.dataset.position === undefined) ?
							", " + NAMES[$("#person-selection").select2("data")[0].element.dataset.position].position :
							(
								(textSelect.dataset.position !== "") ?
								", " + textSelect.dataset.position : ""
							)
						)
					);
				}
				
				await babis.redrawCanvas();
			}
		);
		
		const currentPerson = NAMES[$("#person-selection").select2("data")[0].element.dataset.name];
		
		await babis.setPerson(
			$("#person-selection").select2("data")[0].element.dataset.imageSource,
			true
		);
		
		if (currentPerson.quotes[0].image !== undefined) {
			await babis.setOverlaySource(currentPerson.quotes[0].image, true);
		} else if (currentPerson.image !== undefined) {
			await babis.setOverlaySource(currentPerson.image, true);
		} else {
			await babis.setOverlaySource(null, true);
		}
		
		babis.setNameText(
			(
				(currentPerson.quotes[0].name === undefined) ? 
				currentPerson.name :
				currentPerson.quotes[0].name
			)
			+ (
				(currentPerson.quotes[0].position === undefined) ?
				(
					(currentPerson.position !== "") ?
					", " + currentPerson.position : ""
				) :
				", " + currentPerson.quotes[0].position
			),
			true
		);
		babis.setPrimaryText(currentPerson.quotes[0].quote, true);
		
		$("#reasoning").html(currentPerson.quotes[0].reasoning)
		setTextOptions($("#person-selection").select2("data")[0].element.dataset.name);
		
		babis.redrawCanvas();
	}
); 
